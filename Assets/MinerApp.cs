﻿using System.Diagnostics;
using System.Collections;
using System.Net.Sockets;
using System.Collections.Generic;

public abstract class MinerApp {

    public Process running =null;

    public static List<string[]> miners = new List<string[]>();
    public static List<string[]> pools = new List<string[]>();

    public abstract bool ParseHashrate(string input, graphicsInstance gpu);
    public abstract bool parseSharesAccepted(string input, graphicsInstance gpu);
    public abstract bool parseSharesRejected(string input, graphicsInstance gpu);
    public abstract bool parsePing(string input, graphicsInstance gpu);

    public abstract bool parseEpoch(string input, graphicsInstance gpu);
    public abstract bool parseBlockNumber(string input, graphicsInstance gpu);
    public abstract bool parseDifficulty(string input, graphicsInstance gpu);


    public abstract void start(graphicsInstance gpu);

    public abstract IEnumerator downloadMiner(graphicsInstance gpu);




}
