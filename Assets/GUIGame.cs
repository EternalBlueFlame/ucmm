﻿using UnityEngine;
using System.Collections;

public class GUIGame: MonoBehaviour {
	public static GUIStyle CoreStyle = new GUIStyle();
    public static bool darkMode = true;

    //colors
    public static Color backgroundColor() { return darkMode?new Color(18,18,18):new Color(255,255,255); }
    public static Color primaryColor() { return darkMode ? new Color(29,29,29) : new Color(244,244,244); }
    public static Color secondaryColor() { return darkMode ? new Color(35,35,35) : new Color(220,220,220); }
    public static Color shadeColor() { return darkMode ? new Color(255,255,255,15) : new Color(0,0,0,15); }
    public static Color buttonColor() { return darkMode ? new Color(44,44,44) : new Color(211,211,211); }


    private static Vector2 scrollView = new Vector2(0, 0);
    private static float dropdownOffset = 0;

    private static int overlayGPU = -1;
    private static int overlayID = -1;

    void Start() {

        GUIGame.CoreStyle.alignment = TextAnchor.UpperCenter;
        GUIGame.CoreStyle.fontSize = (int)(Screen.width * 0.025f);
    }

    void OnGUI() {
        GUI.backgroundColor = backgroundColor();
        //show screen parts that are shared
        sidebar();

        //show screens specific to the current menu
        switch (scr_main.menu) {
            case 1: {mainMenu(); break;}
            case 2: {settingsMenu(); break;}

        }

    }

    public void sidebar() {

        //add a GUI button to the side for settings.
        //what it displays is defined by a terinary statement that checks if we are on it's menu or not.
        GUI.backgroundColor = buttonColor();
        if (GUI.Button(scaledRect(0.1f, 0.3f, 0.2f, 0.1f), scr_main.running ? "Stop" : "Start")) {
            foreach (graphicsInstance gpu in scr_main.miners) {
                if (!scr_main.running) {
                    StartCoroutine(gpu.proc.downloadMiner(gpu));
                } else {
                    gpu.proc.running.Kill();
                    gpu.proc.running.WaitForExit();
                    gpu.proc.running = null;
                }
            }
            scr_main.running = !scr_main.running;
        }
        if (GUI.Button(scaledRect(0.1f, 0.45f, 0.2f, 0.1f), scr_main.menu!=2?"Settings":"Mining Monitor")) {
            scr_main.menu=scr_main.menu!=2?2:1;
        }
    }

    public void topBar() {

    }

    //draw the main menu
    public void mainMenu() {

        GUI.backgroundColor = backgroundColor();

        //Define an area, then draw the background box in it.
        GUILayout.BeginArea(scaledRect(0.45f, 0.3f, 0.5f, 0.7f));
        GUILayout.Box("", GUILayout.Width(Screen.width * 0.5f), GUILayout.Height(Screen.height * 0.65f));
        GUILayout.EndArea();

        //Define another area, which will be on top of previous due to order of execution, then add the text to it
        GUILayout.BeginArea(scaledRect(0.45f, 0.3f, 0.65f, 0.7f));
        scrollView = GUILayout.BeginScrollView(scrollView);
        GUILayout.Label(scr_main.eventlog);
        //todo: find a way to get stuff like bar graphs into the view, needs event log split per-card
        GUILayout.EndScrollView();
        GUILayout.EndArea();
    }
    Vector2 scrollbar = new Vector2(0, 0);
    public void settingsMenu() {
        //define the device config menus
        Rect r = scaledRect(0.45f, 0.3f, 0.5f, 0.7f);
        GUILayout.BeginArea(r);
        GUILayout.BeginHorizontal();
        r = scaledRect(0f, 0f, 0.2f, 0.7f);
        GUILayoutOption[] guiOptions = new GUILayoutOption[] {
            GUILayout.MaxWidth(r.width),
            GUILayout.MaxHeight(r.height)
        };

        GUILayoutOption[] o = new GUILayoutOption[]{
            GUILayout.Height(Screen.height*0.03f),
        };
        GUILayout.BeginScrollView(scrollbar, GUIStyle.none, GUIStyle.none, guiOptions);

        foreach (graphicsInstance gpu in scr_main.miners) {
            GUILayout.Label(" ");
            GUILayout.Label("GPU: " + gpu.id + " : " + gpu.name);
            GUI.backgroundColor = buttonColor();
            if (!gpu.isSlave) {
                if (GUILayout.Button(gpu.pool.Equals("null") ? "no pool selected" : gpu.pool, o)) {
                    setOverlay(gpu.id, 1);
                }
                gpu.minerAuguments = GUILayout.TextField(gpu.minerAuguments, o);
                gpu.poolUsername = GUILayout.TextField(gpu.poolUsername, o);
                gpu.poolPassword = GUILayout.TextField(gpu.poolPassword, o);
                gpu.maxTemp = gpu.expertMode ? int.Parse(GUILayout.TextField(gpu.maxTemp + "", o)) :
                Mathf.Min(60, int.Parse(GUILayout.TextField(gpu.maxTemp + "", o)));
                gpu.cooldownTemp = int.Parse(GUILayout.TextField(gpu.cooldownTemp + "", o));
                if (GUILayout.Button(gpu.expertMode ? "Disabled" : "Enabled", o)) {
                    gpu.expertMode = !gpu.expertMode;
                }
                //todo: this should be more dynamic, 40 is the same as intensity 10, which is t-rex minimum, but some can go lower
                gpu.powerScale = Mathf.RoundToInt(
                    GUILayout.HorizontalSlider(gpu.powerScale, 40, 100, o)
                );
            } else {
                if (GUILayout.Button("Paired with GPU: " + gpu.pool, o)) {
                    setOverlay(gpu.id, 1);
                }
            }
        }

        GUILayout.EndScrollView();


        r = scaledRect(0, 0f, 0.2f, 0.7f);
        guiOptions = new GUILayoutOption[] {
            GUILayout.MaxWidth(r.width),
            GUILayout.MaxHeight(r.height)
        };
        scrollbar = GUILayout.BeginScrollView(scrollbar, guiOptions);
        foreach (graphicsInstance gpu in scr_main.miners) {
            GUILayout.Label(" ");
            GUILayout.Label(" ");
            if (!gpu.isSlave) {
                GUILayout.Label("Device pool", o);
                GUILayout.Label("Device auguments", o);
                GUILayout.Label("Device pool username", o);
                GUILayout.Label("Device pool password", o);
                GUILayout.Label("Device max temperature  (Celcius)", o);
                GUILayout.Label("Device cooldown before restart (Celcius)", o);
                GUILayout.Label("Miner temperature safety (WARNING: MAY HARM DEVICE)", o);
                GUILayout.Label("Device mining intensity: " +
                (int)(25f * (gpu.powerScale / 100f)) + " - " + gpu.powerScale + "%", o);
            } else {
                GUILayout.Label("Paired Device/Pool ", o);
            }
        }
        GUILayout.EndScrollView();
        GUILayout.EndHorizontal();
        GUILayout.EndArea();

        if (overlayID ==1) {
            poolMenu();
        } else if (overlayID==2) {
            minerMenu();
        }

    }


    //for the pool selection overlay menu
    private void poolMenu() {
        Vector2 scrollbar2 = new Vector2(0, 0);
        GUILayoutOption[] guiOptions = setLayoutOptions(0.25f, 0.3f);
        GUILayoutOption[] o = new GUILayoutOption[]{
            GUILayout.Height(Screen.height*0.03f),
        };
        popupOverlay();
        //draw actual menu
        GUILayout.BeginArea(scaledRect(0.25f, 0.3f, 0.5f, 0.7f));
        GUILayout.BeginScrollView(scrollbar2, GUIStyle.none, GUIStyle.none, guiOptions);
        GUI.backgroundColor = buttonColor();
        string ogPool = scr_main.miners[overlayGPU].pool;
        scr_main.miners[overlayGPU].pool = GUILayout.TextField(scr_main.miners[overlayGPU].pool, o);

        if (GUILayout.Button("Close Menu", o)) {
            //if it was a slave device, and that has changed, be sure to clean it off the original device
            if (scr_main.miners[overlayGPU].isSlave &&
                ogPool != scr_main.miners[overlayGPU].pool) {
                scr_main.miners[scr_main.parseInt(ogPool)]
                    .slaveDevices.Remove(overlayGPU);
                scr_main.miners[overlayGPU].isSlave = false;
            }
            setOverlay(-1, -1);
        }
        //add known pools
        foreach (string[] pool in MinerApp.pools) {
            if (GUILayout.Button(pool[2], o)) {
                //be sure if it is a slave device, to remove it from the master.
                if (scr_main.miners[overlayGPU].isSlave) {
                    scr_main.miners[scr_main.parseInt(scr_main.miners[overlayGPU].pool)]
                        .slaveDevices.Remove(overlayGPU);
                    scr_main.miners[overlayGPU].isSlave = false;
                }
                scr_main.miners[overlayGPU].pool = pool[2];
                setOverlay(-1, -1);
            }
        }
        //add other cards
        foreach (graphicsInstance pool in scr_main.miners) {
            if (pool.id != overlayGPU && !pool.isSlave) {
                if (GUILayout.Button("Pair with device " + pool.id + ", " + pool.name, o)) {
                    scr_main.miners[overlayGPU].pool = "" + pool.id;
                    scr_main.miners[overlayGPU].isSlave = true;
                    scr_main.miners[pool.id].slaveDevices.Add(overlayGPU);
                    setOverlay(-1, -1);
                }
            }
        }

        GUILayout.EndScrollView();
        GUILayout.EndArea();
    }

    //for the miner application selection overlay menu
    private void minerMenu() {
        Vector2 scrollbar2 = new Vector2(0, 0);
        GUILayoutOption[] guiOptions = setLayoutOptions(0.25f, 0.3f);
        GUILayoutOption[] o = new GUILayoutOption[]{
           GUILayout.Height(Screen.height*0.03f),
        };
        popupOverlay();

        //draw actual menu
        GUILayout.BeginArea(scaledRect(0.25f, 0.3f, 0.5f, 0.7f));
        GUILayout.BeginScrollView(scrollbar2, GUIStyle.none, GUIStyle.none, guiOptions);
        GUI.backgroundColor = buttonColor();
        if (GUILayout.Button("Close Menu", o)) {
            setOverlay(-1, -1);
        }
        //add supported miner tools
        foreach (string[] pool in MinerApp.miners) {
            if (GUILayout.Button(pool[2], o)) {
                if (pool[0].ToLower().Contains("trex")) { 
                    scr_main.miners[overlayGPU].proc = new TRex();
                    scr_main.miners[overlayGPU].minerVersion = pool[0];

                }
                setOverlay(-1,-1);
            }
        }

        GUILayout.EndScrollView();
        GUILayout.EndArea();
    }


    //todo: make another overlay menu for algo selection
    //todo: make algo selection automatic/locked/limited based on the pool address[1] value.


    /*
     * --------------------
     * Shorthands    
     * -------------------- 
     */

    //shorthand for setting the state of the overlay screen, -1 for disabled
    public static void setOverlay(int gpuID, int overlay) {
        overlayGPU = gpuID;
        overlayID = overlay;
        
    }
    //shorthand for making a rect scaled to the screen dimensions.
    public static Rect scaledRect(float x, float y, float width, float height) {
        return new Rect(Screen.width * x, Screen.height * y, Screen.width * width, Screen.height * height);
    }

    //shorthand for making the layout options for individual menu entries
    public static GUILayoutOption[] setLayoutOptions(float width,float height) {
        return new GUILayoutOption[] {
                GUILayout.MaxWidth(Screen.width * width),
                GUILayout.MaxHeight(Screen.height *height)
            };
    }

    //draws the grey-out of the menu
    public static void popupOverlay() {
        //draw background overlay
        GUILayout.BeginArea(scaledRect(0f, 0f, 1f, 1f));
        GUI.backgroundColor = shadeColor();
        GUILayout.Box("", setLayoutOptions(1,1));
        GUILayout.EndArea();
    }


}
