﻿using UnityEngine;

public class Util : MonoBehaviour {

    public static string[] nullString = new string[] { };

	/* unused in this project, but a holdover for convenience, will probably find a use here.
	  public static void renderSliders(graphicsInstance device){
			float x = 0.15f;
			float y = 3;

			resources[i].sliderValue = Mathf.RoundToInt(
				GUI.HorizontalSlider(scaledRect(x, (y * 0.06f), 0.25f, 0.05f), resources[i].sliderValue, 0, resources[i].sliderValue + getSliderUse(resources))
			);
			GUI.Label(scaledRect(x, (y * 0.06f) + 0.025f, 0.25f, 0.5f), getNameByIndex(i) +": "+resources[i].sliderValue + "%", GUIGame.CoreStyle);
	}*/


    //shorthand for making a rect scaled to the screen dimensions.
	public static Rect scaledRect(float x, float y, float width, float height){
		return new Rect(Screen.width * x, Screen.height * y, Screen.width * width, Screen.height * height);
	}

    public static string[] split(string input, string regex) {
        return input.Split(new[] { regex }, System.StringSplitOptions.None);
    }

}

